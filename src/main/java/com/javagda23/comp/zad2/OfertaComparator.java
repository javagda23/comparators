package com.javagda23.comp.zad2;

import java.util.Comparator;

public class OfertaComparator implements Comparator<OfertaSprzedazy> {
    private boolean czyRosnaco;

    public OfertaComparator(boolean czyRosnaco) {
        this.czyRosnaco = czyRosnaco;
    }

    @Override
    public int compare(OfertaSprzedazy o1, OfertaSprzedazy o2) {
        if (o1.getCena() > o2.getCena()) {
            return (czyRosnaco ? 1 : -1); // jeśli czyRosnaca bedzie prawda, to wynik będzie 1, jeśli nie to: -1
        } else if (o1.getCena() < o2.getCena()) {
            return (czyRosnaco ? -1 : 1);
        }
        return 0;
    }
}


//(WARUNEK ? JEŚLI PRAWDA : JEŚLI FAŁSZ)
