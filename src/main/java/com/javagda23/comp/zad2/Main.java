package com.javagda23.comp.zad2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<OfertaSprzedazy> list = new ArrayList<>();
        list.add(new OfertaSprzedazy("a", 1));
        list.add(new OfertaSprzedazy("b", 100));
        list.add(new OfertaSprzedazy("c", 23));
        list.add(new OfertaSprzedazy("d", 57));
        list.add(new OfertaSprzedazy("e", 21));
        list.add(new OfertaSprzedazy("f", 3));
        list.add(new OfertaSprzedazy("g", 8));
        list.add(new OfertaSprzedazy("h", 1));
        list.add(new OfertaSprzedazy("i", 9));
        list.add(new OfertaSprzedazy("j", 13));
        list.add(new OfertaSprzedazy("k", 138));
        list.add(new OfertaSprzedazy("l", 18));
        list.add(new OfertaSprzedazy("m", 98));

        OfertaComparator comparator = new OfertaComparator(false);
        Collections.sort(list, comparator);

        for (OfertaSprzedazy ofertaSprzedazy : list) {
            System.out.println(ofertaSprzedazy);
        }
    }
}
