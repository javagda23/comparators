package com.javagda23.comp.zad1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Osoba> osobaList = new ArrayList<>();

        osobaList.add(new Osoba("a", "b", 5));
        osobaList.add(new Osoba("agy1", "sthb", 23));
        osobaList.add(new Osoba("a2", "tjrb", 3));
        osobaList.add(new Osoba("ashrt", "bh", 13));
        osobaList.add(new Osoba("af", "hb", 17));
        osobaList.add(new Osoba("as", "b", 9));
        osobaList.add(new Osoba("aaa", "bb", 8));

        // wypisanie wszystkich osób w oddzielnych liniach
        for (Osoba osoba : osobaList) {
            System.out.println(osoba);
        }
        System.out.println();

        osobaList.sort(new Comparator<Osoba>() {
            @Override
            public int compare(Osoba o1, Osoba o2) {
                if (o1.getWiek() > o2.getWiek()) {
                    return 1;
                } else if (o1.getWiek() < o2.getWiek()) {
                    return -1;
                }
                return 0;
//                return ((o1.getWiek() > o2.getWiek()) ? 1 : (o1.getWiek() < o2.getWiek() ? -1 : 0));
            }
        });

        // wypisanie wszystkich osób w oddzielnych liniach
        for (Osoba osoba : osobaList) {
            System.out.println(osoba);
        }
    }
}
