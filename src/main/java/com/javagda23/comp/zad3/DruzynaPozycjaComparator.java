package com.javagda23.comp.zad3;

import java.util.Comparator;

public class DruzynaPozycjaComparator implements Comparator<DruzynaPilkarska> {

    @Override
    public int compare(DruzynaPilkarska o1, DruzynaPilkarska o2) {
        if (o1.getPozycjaWRankingu() > o2.getPozycjaWRankingu()) {
            return 1;
        } else if (o1.getPozycjaWRankingu() < o2.getPozycjaWRankingu()) {
            return -1;
        }
        return 0;
    }
}
