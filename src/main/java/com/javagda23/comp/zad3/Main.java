package com.javagda23.comp.zad3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<DruzynaPilkarska> druzynaPilkarskas = new ArrayList<>();

        druzynaPilkarskas.add(new DruzynaPilkarska("a", 1, 8));
        druzynaPilkarskas.add(new DruzynaPilkarska("b", 2, 23));
        druzynaPilkarskas.add(new DruzynaPilkarska("c", 2, 9));

        // SORTOWANIE I WYPISANIE - COMPARATOR 1
        Collections.sort(druzynaPilkarskas, new DruzynaSilaComparator());
        for (DruzynaPilkarska druzynaPilkarska : druzynaPilkarskas) {
            System.out.println(druzynaPilkarska);
        }
        System.out.println();

        // SORTOWANIE I WYPISANIE - COMPARATOR 2
        Collections.sort(druzynaPilkarskas, new DruzynaPozycjaComparator());
        for (DruzynaPilkarska druzynaPilkarska : druzynaPilkarskas) {
            System.out.println(druzynaPilkarska);
        }
    }
}
