package com.javagda23.comp.zad3;

import java.util.Comparator;

public class DruzynaSilaComparator implements Comparator<DruzynaPilkarska> {
    @Override
    public int compare(DruzynaPilkarska o1, DruzynaPilkarska o2) {
        double o1Proporcja = o1.getSilaDruzyny() / o1.getPozycjaWRankingu();
        double o2Proporcja = o2.getSilaDruzyny() / o2.getPozycjaWRankingu();

        if (o1Proporcja > o2Proporcja) {
            return -1;
        } else if (o1Proporcja < o2Proporcja) {
            return 1;
        }
        return 0;
    }
}
